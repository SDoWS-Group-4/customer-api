package dtu;

import java.util.UUID;

import lombok.Value;

@Value
public class CorrelationId {
	private UUID id;

	public CorrelationId(UUID randomUUID) {
		id = randomUUID;
	}

	public static CorrelationId randomId() {
		return new CorrelationId(UUID.randomUUID());
	}
}
