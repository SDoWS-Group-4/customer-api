package dtu.api;

import java.util.List;
import java.util.NoSuchElementException;

import javax.ws.rs.*;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dtu.Customer;
import dtu.CustomerRegistrationService;
import dtu.Payment;
import dtu.Exceptions.MyBadRequestException;
import dtu.Exceptions.MyNotFoundException;
import dtu.adapter.rest.CustomerAdapter;
import dtu.adapter.rest.CustomerRegistrationFactory;

@Path("/customer")
public class CustomerResource {

    CustomerAdapter customerAdapter = new CustomerAdapter();

    @Path("/register")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public String registerCustomer(Customer customer) throws MyBadRequestException {
        try {
            return customerAdapter.register(customer);
        } catch (IllegalArgumentException e) {
            throw new MyBadRequestException(e.getMessage());
        } catch (Exception ex) {
            throw new MyBadRequestException(ex.getMessage());
        }
    }


    @Path("/deregister")
    @DELETE
    public String deregisterCustomer(@QueryParam("id") String id) throws MyBadRequestException, MyNotFoundException {
        try {
            return customerAdapter.deregister(id);
        } catch (IllegalArgumentException e) {
            throw new MyBadRequestException(e.getMessage());
        } catch (NoSuchElementException ex) {
            throw new MyNotFoundException(ex.getMessage());
        } catch (Exception ex1) {
            throw new MyBadRequestException(ex1.getMessage());
        } 
    }

    @Path("/report")
    @GET
    @Produces("application/json")
    public List<Payment> getReportAPI(@QueryParam("id") String id) throws MyBadRequestException, MyNotFoundException {
        try {
        return customerAdapter.getReport(id);
        } catch (NoSuchElementException ex) {
            throw new MyNotFoundException(ex.getMessage());
        } catch (IllegalArgumentException ex1) {
            throw new MyBadRequestException(ex1.getMessage()); 
        } catch (Exception e) {
            throw new MyBadRequestException(e.getMessage());
        }
    }

    @Path("/token")
    @GET
    public List<String> getTokens(@QueryParam("id") String id, @QueryParam("amount") String amount) throws MyBadRequestException {
        List<String> tokens;
        try {
            tokens = customerAdapter.getTokens(id, amount);
            return tokens;
        } catch (IllegalArgumentException e) {
            throw new MyBadRequestException(e.getMessage());
        } catch(Exception ex){
            throw new MyBadRequestException(ex.getMessage());
        }   
    }
}
