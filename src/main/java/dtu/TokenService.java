package dtu;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.GenericType;

import messaging.Event;
import messaging.MessageQueue;

public class TokenService {

	private MessageQueue queue;
	private Map<CorrelationId, CompletableFuture<List<String>>> correlations = new ConcurrentHashMap<>();

	public TokenService(MessageQueue q) {
		queue = q;
		queue.addHandler("TokensGenerated", this::handleTokensGenerated);
		queue.addHandler("TokensNotGenerated", this::handleTokensNotGenerated);
	}

    public List<String> getTokens(String id, String amount) throws Exception {
        try {
			var correlationId = CorrelationId.randomId();
			correlations.put(correlationId, new CompletableFuture<>());
			Event event = new Event("GenerateTokenRequest", new Object[] { id, amount, correlationId });
			queue.publish(event);
			return correlations.get(correlationId).join();
		} catch (Exception e) {
			e.printStackTrace();
			if (e.getCause() != null) {
				if (e.getCause().getClass() == IllegalArgumentException.class) {
					throw new IllegalArgumentException(e.getCause().getMessage());
				}
			}
			throw new Exception(e.getMessage());
		}
	}

	private void handleTokensGenerated(Event e) {
		ArrayList<String> tokens = e.getArgument(0, ArrayList.class);
		var cid = e.getArgument(1, CorrelationId.class);

		correlations.get(cid).complete(tokens);
	}

	private void handleTokensNotGenerated(Event e) {
		IllegalArgumentException exception = e.getArgument(0, IllegalArgumentException.class);
		var cid = e.getArgument(1, CorrelationId.class);
		// correlations.get(cid).complete(exception);
		correlations.get(cid).completeExceptionally(exception);
	}
}
