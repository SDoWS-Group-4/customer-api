package dtu;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import messaging.Event;
import messaging.MessageQueue;

public class CustomerRegistrationService {

	private MessageQueue queue;
	private Map<CorrelationId, CompletableFuture<String>> correlations = new ConcurrentHashMap<>();

	public CustomerRegistrationService(MessageQueue q) {
		queue = q;
		queue.addHandler("CustomerIdAssigned", this::handleCustomerIdAssigned);
		queue.addHandler("CustomerNotAssigned", this::handleCustomerNotAssigned);

		queue.addHandler("CustomerDeregistered", this::handleCustomerDeregistered);
		queue.addHandler("CustomerNotDeregistered", this::handleCustomerNotDeregistered);
	}

	public String register(Customer c) throws Exception {
		try {
			var correlationId = CorrelationId.randomId();
			correlations.put(correlationId, new CompletableFuture<>());
			Event event = new Event("CustomerRegistrationRequested", new Object[] { c, correlationId });
			queue.publish(event);

			return correlations.get(correlationId).join();
		} catch (Exception e) {
			e.printStackTrace();
			if (e.getCause() != null) {
				if (e.getCause().getClass() == IllegalArgumentException.class) {
					throw new IllegalArgumentException(e.getCause().getMessage());
				}
			}
			throw new Exception(e.getMessage());
		}
	}

	public String deregister(String id) throws Exception {
		try {
			var correlationId = CorrelationId.randomId();
			correlations.put(correlationId, new CompletableFuture<>());
			Event event = new Event("CustomerDeregistrationRequested", new Object[] { id, correlationId });
			queue.publish(event);

			return correlations.get(correlationId).join();
		} catch (Exception e) {
			e.printStackTrace();
			if (e.getCause() != null) {
				if (e.getCause().getClass() == IllegalArgumentException.class) {
					throw new IllegalArgumentException(e.getCause().getMessage());
				}
				if (e.getCause().getClass() == NoSuchElementException.class) {
					throw new NoSuchElementException(e.getCause().getMessage());
				}
				if (e.getCause().getClass() == Exception.class) {
					System.out.println("HER");
                    throw new Exception(e.getMessage());
                }
			}
			throw new Exception(e.getCause().getMessage());
		}
	}

	public void handleCustomerIdAssigned(Event e) {
		var customerId = e.getArgument(0, String.class);
		var cid = e.getArgument(1, CorrelationId.class);

		correlations.get(cid).complete(customerId);
	}

	public void handleCustomerNotAssigned(Event e) {
		var exception = e.getArgument(0, IllegalArgumentException.class);
		var cid = e.getArgument(1, CorrelationId.class);

		// correlations.get(cid).complete(exception);
		correlations.get(cid).completeExceptionally(exception);
	}

	public void handleCustomerDeregistered(Event e) {
		var s = e.getArgument(0, String.class);
		var c = e.getArgument(1, CorrelationId.class);

		correlations.get(c).complete(s);
	}

	public void handleCustomerNotDeregistered(Event e) {
		var exception = e.getArgument(0, NoSuchElementException.class);
		var cid = e.getArgument(1, CorrelationId.class);

		// correlations.get(cid).complete(exception);
		correlations.get(cid).completeExceptionally(exception);
	}
}
