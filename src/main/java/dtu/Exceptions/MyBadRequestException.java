package dtu.Exceptions;

public class MyBadRequestException extends Exception {

    private static final long serialVersionUID = 1L;

    public MyBadRequestException() {
        super();
    }
    
    public MyBadRequestException(String msg)   {
        super(msg);
    }   

    public MyBadRequestException(String msg, Exception e)  {
        super(msg, e);
    }
}
