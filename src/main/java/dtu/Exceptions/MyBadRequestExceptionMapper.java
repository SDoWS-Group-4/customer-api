package dtu.Exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MyBadRequestExceptionMapper implements ExceptionMapper<MyBadRequestException> {

    @Override
    public Response toResponse(MyBadRequestException exception) {
        return Response.status(Status.BAD_REQUEST).entity(exception.getMessage()).build();
    }
}
