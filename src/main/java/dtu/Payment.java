package dtu;

import org.junit.runner.notification.RunListener.ThreadSafe;

public class Payment {
    private String id;
    private String sender;
    private String receiver;
    private Double bal;

    public Payment() {

    }

    public Payment(String id, String sender, String receiver, Double bal) {
        this.id = id;
        this.sender = sender;
        this.receiver = receiver;
        this.bal = bal;
    }

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public Double getBalance() {
        return bal;
    }

    public void setBalance(Double bal) {
        this.bal = bal;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    @Override
    public String toString() {
        
        return "sender:" + this.sender + ", receiver:" + this.receiver + ", balance:" + this.bal;
    }

    @Override
    public boolean equals(Object o) {
        // If the object is compared with itself then return true 
        if (o == this) {
            return true;
        }
        Payment p = (Payment) o;
        return p.toString().equals(this.toString());
    }
}
