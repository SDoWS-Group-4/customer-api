package dtu.adapter.rest;

import java.util.ArrayList;
import java.util.List;

import dtu.Customer;
import dtu.CustomerRegistrationService;
import dtu.Payment;
import dtu.PaymentService;
import dtu.TokenService;

public class CustomerAdapter {
    CustomerRegistrationService service = new CustomerRegistrationFactory().getService();
    TokenService tokenService = new TokenServiceFactory().getService();
    PaymentService paymentService = new PaymentServiceFactory().getService();

    public void pay(Payment pay) throws Exception {

        // Rabbit mq with payment
    }

    public List<Payment> getReport(String id) throws Exception {
        if (id.isEmpty() || id == null) {throw new IllegalArgumentException("Missing fields");}
        return paymentService.getReport(id);
    }

    public List<String> getTokens(String id, String amount) throws Exception {
        if (Integer.parseInt(amount) > 5) {
            throw new IllegalArgumentException("Please request 1-5 tokens");
        } else if (Integer.parseInt(amount) < 1) {
            throw  new IllegalArgumentException("Please request an amount of tokens greater than zero");
        }
        return tokenService.getTokens(id, amount);
    }

    public String register(Customer customer) throws Exception {
        if (isNullOrEmpty(customer)) {throw new IllegalArgumentException("Missing fields");}

            return service.register(customer);
    }

    public String deregister(String id) throws Exception {
            if (id.isEmpty() || id == null) {throw new IllegalArgumentException("Missing fields");}
            return service.deregister(id);
    }

    private Boolean isNullOrEmpty(Customer customer){
        return customer.getBankAccount() == null ||
        customer.getCprNumber() == null ||
        customer.getFirstName() == null ||
        customer.getLastName() == null ||
        customer.getBankAccount().isBlank() ||
        customer.getCprNumber().isBlank() ||
        customer.getFirstName().isBlank() ||
        customer.getLastName().isBlank();
    }
}
