package dtu;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.Data;
@Data
public class Customer implements Serializable {

    @Getter @Setter private String id;
    @Getter @Setter private String firstName;
    @Getter @Setter private String lastName;
    @Getter @Setter private String cprNumber;
    @Getter @Setter private String bankAccount;

    public Customer() {
    }

    public Customer(String id, String firstName, String lastName, String cprNumber) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.cprNumber = cprNumber;
    }
}
