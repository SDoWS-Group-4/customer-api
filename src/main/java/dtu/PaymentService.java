package dtu;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import messaging.MessageQueue;
import messaging.MessageQueue;
import messaging.Event;


public class PaymentService {

    private MessageQueue queue;
	private Map<CorrelationId, CompletableFuture<List<Payment>>> correlations = new ConcurrentHashMap<>();

    public PaymentService(MessageQueue q) {
        queue = q;
		queue.addHandler("CustomerPaymentsFound", this::handleCustomerPaymentsFound);
		queue.addHandler("CustomerPaymentsNotFound", this::handleCustomerPaymentsNotFound);
    }

    public List<Payment> getReport(String id) throws Exception {
        try {
			var correlationId = CorrelationId.randomId();
			correlations.put(correlationId, new CompletableFuture<>());
			Event event = new Event("CustomerPaymentsRequested", new Object[] { id, correlationId });
			queue.publish(event);
			return correlations.get(correlationId).join();
		} catch (Exception e) {
			e.printStackTrace();
			if (e.getCause() != null) {
				if (e.getCause().getClass() == NoSuchElementException.class) {
					throw new IllegalArgumentException(e.getCause().getMessage());
				}
			}
			throw new Exception(e.getMessage());
		}
	}

    private void handleCustomerPaymentsFound(Event ev) {
        ArrayList<Payment> report = ev.getArgument(0, ArrayList.class);
		var cid = ev.getArgument(1, CorrelationId.class);

		correlations.get(cid).complete(report);
    }

    private void handleCustomerPaymentsNotFound(Event ev) {
        NoSuchElementException exception = ev.getArgument(0, NoSuchElementException.class);
		var cid = ev.getArgument(1, CorrelationId.class);
		// correlations.get(cid).complete(exception);
		correlations.get(cid).completeExceptionally(exception);
	}
}
