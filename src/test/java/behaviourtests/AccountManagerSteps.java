package behaviourtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import dtu.CorrelationId;
import dtu.Customer;
import dtu.CustomerRegistrationService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;

//public class AccountManagerSteps {
//
//    // private CompletableFuture<Event> publishedEvent = new CompletableFuture<>();
//	private Map<String,CompletableFuture<Event>> publishedEvents = new HashMap<>();
//
//	private MessageQueue q = new MessageQueue() {

//		@Override
//		public void publish(Event event) {
//			Customer customer = event.getArgument(0, Customer.class);
//			publishedEvents.get(customer.getName()).complete(event);
//		}
//
//		@Override
//		public void addHandler(String eventType, Consumer<Event> handler) {
//		}
//
//	};
//	private CustomerRegistrationService service = new CustomerRegistrationService(q);
//    private CompletableFuture<Customer> registeredCustomer = new CompletableFuture<>();
//	private Map<Customer,CorrelationId> correlationIds = new HashMap<>();
//	private Customer customer;

//
//    @Given("there is a Customer with empty id")
//    public void thereIsACustomerWithEmptyId() {
//        customer = new Customer();
//        customer.setName("Jane Doe");
//        publishedEvents.put(customer.getName(), new CompletableFuture<Event>());
//        assertNull(customer.getId());
//    }
//
//    @Given("the Customer has a bankaccount in the bank")
//    public void theCustomerHasABankaccountInTheBank() {
//        customer.setBankAccount("1234567890");
//    }
//
//    @When("the Customer is being registered")
//    public void theCustomerIsBeingRegistered() {
//        new Thread(() -> {
//			Customer customer = service.register(this.customer);
//			registeredCustomer.complete(customer);
//		}).start();
//    }
//
//    @Then("the {string} event is sent")
//	public void theEventIsSent(String string) {
//        Event event = publishedEvents.get(this.customer.getName()).join();
//		assertEquals(string,event.getType());
//		Customer customer = event.getArgument(0, Customer.class);
//		CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
//		correlationIds.put(customer, correlationId);
//        // Event event = new Event(string, new Object[] { customer });
//	}
//
//    @When("the {string} event is received")
//    public void theEventIsReceived(String string) {
//        Customer customer = new Customer();
//		customer.setName(this.customer.getName());
//		customer.setId("123");
//		service.handleCustomerIdAssigned(new Event("CustomerIdAssigned",new Object[] {customer, correlationIds.get(this.customer)}));
//	 }
//
//    @Then("the Customer is registered and his id is set")
//    public void theCustomerIsRegisteredAndHisIdIsSet() {
//        assertNotNull(registeredCustomer.join().getId());
//    }
//}
// 	private CompletableFuture<Event> publishedEvent = new CompletableFuture<>();
	
// 	private MessageQueue q = new MessageQueue() {

// 		@Override
// 		public void publish(Event event) {
// 			publishedEvent.complete(event);
// 		}

// 		@Override
// 		public void addHandler(String eventType, Consumer<Event> handler) {
// 		}
		
// 	};
// 	private CustomerRegistrationService service = new CustomerRegistrationService(q);
// 	private CompletableFuture<Customer> registeredCustomer = new CompletableFuture<>();
// 	private Customer customer;

// 	public CustomerRegistrationSteps() {
// 	}

// 	@Given("there is a customer with empty id")
// 	public void thereIsACustomerWithEmptyId() {
// 		customer = new Customer();
// 		customer.setName("James");
// 		assertNull(customer.getId());
// 	}

// 	@When("the customer is being registered")
// 	public void theCustomerIsBeingRegistered() {
// 		// We have to run the registration in a thread, because
// 		// the register method will only finish after the next @When
// 		// step is executed.
// 		// step is executed.
// 		new Thread(() -> {
// 			var result = service.register(customer);
// 			registeredCustomer.complete(result);
// 		}).start();
// 	}

// 	@Then("the {string} event is sent")
// 	public void theEventIsSent(String string) {
// 		Event event = new Event(string, new Object[] { customer });
// 		assertEquals(event,publishedEvent.join());
// 	}

// 	@When("the {string} event is sent with non-empty id")
// 	public void theEventIsSentWithNonEmptyId(String string) {
// 		// This step simulate the event created by a downstream service.
// 		var c = new Customer();
// 		c.setName(customer.getName());
// 		c.setId("123");
// 		service.handleCustomerIdAssigned(new Event("..",new Object[] {c}));
// 	}

// 	@Then("the customer is registered and his id is set")
// 	public void theCustomerIsRegisteredAndHisIdIsSet() {
// 		// Write code here that turns the phrase above into concrete actions
// 		throw new io.cucumber.java.PendingException();
// 	}
