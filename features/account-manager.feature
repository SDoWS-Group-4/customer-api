################# Event Names #########################
# Event(s) -> CustomerRegistrationRequested
# Event(s) <-  CustomerIdAssigned,CustomerIdNotAssigned

# Event(s) -> MerchantRegistrationRequested
# Event(s) <-  MerchantIdAssigned,MerchantIdNotAssigned

#Feature: Account manager feature

# Scenario: Customer account registered
# 	Given there is a Customer with empty id
#    And the Customer has a bankaccount in the bank
# 	When the Customer is being registered
# 	Then the "CustomerRegistrationRequested" event is sent
# 	When the "CustomerIdAssigned" event is received
# 	Then the Customer is registered and his id is set

#  Scenario: Customer account not registered due to not being initialised
#  	When the Customer is being registered
#  	Then the "CustomerRegistrationRequested" event is sent                                    
#  	When the "CustomerNotAssigned" event is received
#  	Then the Customer is not registered and his id is not set


#  Scenario: Customer account not registered due to no bankaccount
#  	Given there is a Customer with empty id
#     And the customer does not have a bankaccount 
#  	When the Customer is being registered
#  	Then the "CustomerRegistrationRequested" event is sent                                    
#  	When the "CustomerNotAssigned" event is received
#  	Then the Customer is not registered and his id is not set


# Scenario: Customer account not registered due to bankaccount not in Bank: 
#     Given there is a Customer with empty id 
#     And the customer does not have a bankaccount in the bank, #assertequals, tjek listen af accounts igennem for bank, se at ingen er lig med customer acccount
#     When the Customer is being registered
#  	Then the "CustomerRegistrationRequested" event is sent                                    
#  	When the "CustomerNotAssigned" event is received
#  	Then the Customer is not registered and his id is not set





     # -> CustomerRegistrationRequested
     # -> MerchantRegistrationRequested
     
     # <- AccountRegistered / AccountNotRegistered
     # (User user, BigDecimal amount) 